# <span style="color:darkblue">Modélisation de données avec Merise<span>


**MERISE** est une méthode d'analyse et de conception des systèmes informatique basée sur le principe de la séparation des données et des traitements. Elle possède un certain nombre de modèles (ou schémas) qui sont répartis sur trois niveaux :

- le niveau conceptuel ;
- le niveau logique ou organisationnel ;
- le niveau physique.

---------------------------
## <span style="color:darkblue">Modélisation d'une base de données au niveau conceptuel<span>


Il s'agit de l'élaboration du modèle conceptuel des données (MCD) qui est une représentation graphique et structurée des informations mémorisées par un système informatique. Le MCD est basé sur deux notions principales : les entités et les associations.

L'élaboration du MCD passe par les étapes suivantes :

- la mise en place de règles de gestion (si nous partons d'un page blanche) ;
- l'élaboration du dictionnaire des données ;
- la recherche des dépendances fonctionnelles entre ces données ;
- l'élaboration du MCD (création des entités puis des associations puis ajout des cardinalités).


---------------------------
## <span style="color:darkblue"> Modélisation au niveau logique et passage au SQL<span>

Le modèle logique de données (MLD) est composé uniquement de ce que l'on appelle des relations. Ces relations sont à la fois issues des entités du Modèle conceptuel de données (MDC), mais aussi d'associations, dans certains cas. Ces relations nous permettront par la suite de créer nos tables au niveau physique.

Une relation est composée d'attributs. Ces attributs sont des données élémentaires issues des propriétés des différentes entités, mais aussi des identifiants et des données portées par certaines associations.

Une relation possède un nom qui correspond en général à celui de l'entité ou de l'association qui lui correspond. Elle possède aussi une **clef primaire** qui permet d'identifier sans ambiguïté chaque occurrence de cette relation. La clef primaire peut être composée d'un ou plusieurs attributs, il s'agit d'une implantation de la notion d'identifiant des entités et associations qui se répercute au niveau relationnel.

Il existe un autre type de clef appelé **clef étrangère**. La clef étrangère est un attribut d'une relation qui fait référence à la clef primaire d'une autre relation (ces deux clefs devront donc avoir le même type de données). 

```
exemple:
id_commande clé primaire
id_client clé étrangère
```
```{image} ./figures/1_N.png
:alt: 1 à N
:width: 650px
:align: center
``` 

Afin de modéliser notre modèle il est important de se poser quelques questions et définir des règles. Nous parlons de **Cardinalités**

Ce sont des couples de valeur qui apportent des contraintes sur le MCD. Les cardinalités sont surtout importantes car elles traduisent des règles de gestion. Elles doivent être validées avec l’utilisateur final.

Les cardinalités sont des caractères (0,1, n) qui fonctionnent par couple et qui sont présents de chaque côté d’une association. Les cardinalités donnent des indications très intéressantes et permettent par la suite de construire la base de données : par exemple la création des clés étrangères (vu précédemment)

Certaines cardinalités sont évidentes. Dans la plupart des cas, il faudra demander confirmation aux utilisateurs. Ce sont eux qui sont capables de préciser les règles de gestion et la façon dont le métier doit être informatisé. Le même cas de figure peut ainsi avoir des implémentations différentes suivant les points de vue ou les entreprises. Pour bien définir les cardinalités, il faut simplement poser des questions. Par exemple cela peut être :

    Une structure se situe-t-elle dans une seule ville ? Ou bien peut-elle être présente dans plusieurs villes ?
    Une ville peut-elle héberger plusieurs structures ?

---------------------------


### <span style="color:black"> Cardinalités de type 1,1 <span>

Lorsque deux entités sont toutes deux reliées avec une cardinalité 1,1 par une même association, on peut placer la clef étrangère de n'importe quel côté. Par convention, on choisit de la placer du côté de la relation correspondant à l'entité ayant le plus de liaisons avec les autres. Avoir une cardinalité de 1, 1 permet de fusionner les deux côtés de cette dernière. Ce qui nous montre la simplicité de la relation. Cependant il n'est pas forcément recommandé de le faire si nous voulons par exemple conserver l'état actuel de la table.

```{image} ./figures/1_1.png
:alt: 1 à 1
:width: 650px
:align: center
``` 

### <span style="color:black"> Cardinalités de type 0 ou 1 , N <span>

Ce sont les associations où d'un côté la cardinalité maximale est à **1** et de l'autre côté la cardinalité maximale est à **n**

```{image} ./figures/1_N.png
:alt: 1 à N
:width: 650px
:align: center
``` 

Cela signifie qu'une occurrence de A est reliée au plus à une seule occurrence de B. C'est-à-dire si on connaît une occurrence de A alors on saura forcément quelle est la seule occurrence de B qui correspond (si elle existe). On dit que A détermine B. C'est un lien de dépendance fonctionnelle. B dépend fonctionnellement de A.



---------------------------

## <span style="color:darkblue"> Cas particulier <span>

**Les associations réflexives**

Il est possible de relier une entité à elle-même par une association, on parle dans ce cas-là d'association réflexive. Imaginons que l'on veuille connaître les inscrits qui sont mariés entre eux tout en conservant leur date de mariage, voici ce que l'on obtiendrait au niveau conceptuel :

```{image} ./figures/particuliers_1.png
:alt: Cas particuliers
:width: 650px
:align: center
``` 
