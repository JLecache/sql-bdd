# <span style="color:darkblue">SQL comme standard<span>

Dans cette partie nous allons voir comment construire des requêtes SQL afin d'intéragir avec la base de données.

Savoir effectuer des requêtes n’est pas trop complexe, mais il convient de véritablement comprendre comment fonctionne le stockage des données et la façon dont elles sont lues pour optimiser les performances. Les optimisations sont basées dans 2 catégories: les bons choix à faire lorsqu’il faut définir la structure de la base de données et les méthodes les plus adaptées pour lire les données.

---------------------------
## <span style="color:darkblue">Création d'une table<span>
```sql
CREATE TABLE nom_de_la_table
(
    colonne1 type_donnees,
    colonne2 type_donnees,
    colonne3 type_donnees,
    colonne4 type_donnees
)
```
Le mot-clé “type_donnees” sera à remplacer par un mot-clé pour définir le type de données (INT, DATE, TEXT …).

Il est également possible de définir des options qui s'applique spécialement pour la colonne: 

NOT NULL : empêche d’enregistrer une valeur nulle pour une colonne.
DEFAULT : attribuer une valeur par défaut si aucune données n’est indiquée pour cette colonne lors de l’ajout d’une ligne dans la table.
PRIMARY KEY : indiquer si cette colonne est considérée comme clé primaire pour un index.


Exemple de la création d'une table 

```sql
CREATE TABLE schema.utilisateur
(
    id INT PRIMARY KEY NOT NULL,
    nom VARCHAR(100),
    prenom VARCHAR(100),
    email VARCHAR(255),
    date_naissance DATE,
    pays VARCHAR(255),
    ville VARCHAR(255),
    code_postal VARCHAR(5),
    nombre_achat INT
)
```

## <span style="color:darkblue">Sélection au sein de la DB<span>

### <span style="color:black">SELECT<span>


L'utilisation la plus courante du SQL consiste à lire et interrogger la base de données. Le tout s'effectue avec la commande ***SELECT***, cette sélection retourne les résultats dans un tableau. Avec cette commande nous povons interrogger une ou plusieurs colonne de la table. 


Sélection d'une colonne:
```sql
SELECT nom_du_champ FROM schema.nom_du_tableau;
```


Sélection de plusieurs colonnes:
```sql
SELECT nom_du_champ1, nom_du_champ2, nom_du_champ3  FROM schema.nom_du_tableau;
```

Sélection de toute les colonnes de la table:
```sql
SELECT * FROM schema.nom_du_tableau;
```

### <span style="color:black">WHERE<span>

Ajout d'une condition après le from
```sql
SELECT nom_colonnes FROM schema.nom_table WHERE condition;
```

exemple:
```sql
SELECT * FROM schema.client WHERE ville = 'cergy';
```

### <span style="color:black">AND & OR<span>

L'opérateur **AND** permet d'ajouter une ou plusieurs conditions à notre requête 

```sql
SELECT nom_colonnes FROM schema.nom_table WHERE condition1 AND condition2
```
L’opérateur **OR** vérifie quant à lui que la condition1 ou la condition2 est vrai :

```sql
SELECT nom_colonnes FROM schema.nom_table WHERE condition1 OR condition2
```

### <span style="color:black">IN<span>

L’opérateur logique IN dans SQL  s’utilise avec la commande WHERE pour vérifier si une colonne est égale à une des valeurs comprise dans set de valeurs déterminés.

```sql
SELECT prenom
FROM schema.utilisateur
WHERE prenom IN ( 'Maurice', 'Marie', 'Thimoté' )
```

Cette opérateur est aussi plus simple d'utilisation pour vérifier la présence de valeurs que l'utilisation de **OR**. Avec la requête suivante nous obtenons les mêmes résultats cependant elle est moins simple en écriture. 
```sql
SELECT prenom
FROM schema.utilisateur
WHERE prenom = 'Maurice' OR prenom = 'Marie' OR prenom = 'Thimoté'
```

L'ajout de **NOT IN** permet d'ajouter la condition inverse, la nonm présence de valeurs. Comme l'exemple suivant nous retourne tout les prénoms de client qui ne sont pas dans les villes de Cergy, Paris et Nanterre. 

```sql
SELECT prenom
FROM schema.client
WHERE ville NOT IN ( 'cergy', 'paris', 'nanterre' )
```

### <span style="color:black">BETWEEN<span>

L'opérateur BETWEEN permet de sélection des données dans un intervalle. L’intervalle peut être constitué de chaînes de caractères, de nombres ou de dates.
```sql
SELECT *
FROM schema.utilisateur
WHERE id BETWEEN 4 AND 10
```

### <span style="color:black">LIKE<span>

Cet opérateur permet de rechercher des valeurs sur un modèle particulier. Par exemple des valeurs d'une colonne qui commence par telle lettre ou terminant par une autre. 

Par exemple nous obtenons les villes qui commençent la lettre "N"
```sql
SELECT *
FROM schema.client
WHERE ville LIKE 'N%'
```
Ici les villes se terminant par la lettre "e"
```sql
SELECT *
FROM schema.client
WHERE ville LIKE '%e'
```

---------------------------
## <span style="color:darkblue">Insertion au sein de la DB<span>

Insérer une ligne en spécifiant toutes les colonnes

```sql
INSERT INTO table VALUES ('valeur 1', 'valeur 2', ...)
```
Cette méthode d'insertion permet d'obliger de remplir toutes les données, tout en respectant l’ordre des colonnes. Il n’y a pas le nom de colonne, donc les fautes de frappe sont limitées. Par ailleurs, les colonnes peuvent être renommées sans avoir à changer la requête. L’ordre des colonnes doit resté identique sinon certaines valeurs prennent le risque d’être complétée dans la mauvaise colonne


Insérer une ligne en spécifiant seulement les colonnes souhaitées
```sql
INSERT INTO table (nom_colonne_1, nom_colonne_2, ...
 VALUES ('valeur 1', 'valeur 2', ...)
```

Cette méthode est similaire que la première et permet de spécifier que certaines colonnes, de plus l'ordre des colonnes n'est pas important.

Insertion de plusieurs lignes à la fois

```sql
INSERT INTO client (prenom, nom, ville, age)
 VALUES
 ('Rébecca', 'Armand', 'Saint-Didier-des-Bois', 24),
 ('Aimée', 'Hebert', 'Marigny-le-Châtel', 36),
 ('Marielle', 'Ribeiro', 'Maillères', 27),
 ('Hilaire', 'Savary', 'Conie-Molitard', 58);
```

Lorsque le champ à remplir est de type VARCHAR ou TEXT il faut indiquer le texte entre guillemet simple. En revanche, lorsque la colonne est un numérique tel que integer ou BIGINT il n’y a pas besoin d’utiliser de guillemet, il suffit juste d’indiquer le nombre.

Insertion de plusieurs lignes à la fois via un select

```sql
INSERT INTO villes (nom, code_insee, code_postal)
SELECT nom_ville, insee, cp
FROM schema.ville
```

Il est possible de la même manière précédente d'insérer plusieurs lignes en spécifiant les insertions provenant d'un autre table. L'ordre des colonnes de la table destination ne doit pas spécialement être respecter, cepepandant l'ordre des colonnes entre le "INSERT INTO" et "SELECT" doit l'être.

---------------------------
## <span style="color:darkblue">Mettre à jour des valeurs<span>
La commande UPDATE permet d’effectuer des modifications sur des lignes existantes. Très souvent cette commande est utilisée avec WHERE pour spécifier sur quelles lignes doivent porter la ou les modifications.
Pour spécifier en une seule fois plusieurs modification, il faut séparer les colonnes par des virgules. 
```sql
UPDATE schema.table
SET nom_colonne_1 = 'nouvelle valeur'
WHERE condition
```

Afin de modifier toutes les lignes sans conditions, la syntaxe est la suivante:

```sql
UPDATE schema.client
SET ville = 'Cergy'
```

## <span style="color:darkblue">Suppression des valeurs<span>

La commande DELETE en SQL permet de supprimer des lignes dans une table. 

```sql
DELETE FROM schema.client WHERE pays ='FRANCE'
```
Avant d’essayer de supprimer des lignes, il est recommandé d’effectuer une sauvegarde de la base de données, ou tout du moins de la table concernée par la suppression. Ainsi, s’il y a une mauvaise manipulation il est toujours possible de restaurer les données.

    Attention : s’il n’y a pas de condition WHERE alors toutes les lignes seront supprimées et la table sera alors vide.

La commende TRUNCATE effectue la même action que la commande DELETE sans condition WHERE. Cependant il y a une différence de rapidité et utilisation moindre des ressources. TRUNCATE va ré-initialiser la valeur de l’auto-incrément, s’il y en a un.

```sql
TRUNCATE TABLE schema.client
```
## <span style="color:darkblue">Utilisation des sous-requêtes<span>

Une sous-requête est une requête dans une autre requête. Les sous-requêtes dans une clause WHERE sont utilisé pour fournir des données qui seront utilisées pour limiter ou comparer/évaluer les données renvoyées par la requête les contenant.

IN

```sql
SELECT client
FROM schema.reservation
WHERE client IN (SELECT client FROM schema.restaurant WHERE client.reservation = client.restaurant);
```

L'opérateur IN permet renvoyer les valeurs qui se situe au sein de la sous-requête, ici connaitre les clients de la table réservation qui se trouve au sein de la table restaurant.

NOT IN

```sql
SELECT client
FROM schema.reservation
WHERE client NOT IN (SELECT client FROM schema.restaurant WHERE client.reservation = client.restaurant);
```

A l'inverse l'opérareur NOT IN permet de connaitre les valeurs qui ne sont pas présent au sein de la sous-requête.

EXISTS

L'argument d'EXISTS est une instruction SELECT arbitraire ou une sous-requête. La sous-requête est évaluée pour déterminer si elle renvoie des lignes. Si elle en renvoie au moins une, le résultat d'EXISTS est vrai (« true ») ; si elle n'en renvoie aucune, le résultat d'EXISTS est faux (« false »). 

```sql
SELECT client
FROM schema.reservation
WHERE client EXISTS (SELECT client FROM schema.restaurant WHERE client.reservation = client.restaurant);
```

Il est aussi possible les opérateurs suivants afin de constuire notre sous-requête:

| Opérateur  | Description          |
| :--------------- |:---------------:|
| < |    	inférieur à        |
| >  | supérieur à             |
| <=  | inférieur ou égal à          |
| >=  | supérieur ou égal à         |
| = | égal à          |
| <> ou !=  | différent de          |

## Utilisaion du WITH pour une sous-requête

WITH fournit un moyen d'écrire des ordres auxiliaires pour les utiliser dans des requêtes plus importantes. Ces requêtes, qui sont souvent appelées Common Table Expressions ou CTE, peuvent être vues comme des tables temporaires qui n'existent que pour une requête. Chaque ordre auxiliaire dans une clause WITH peut être un SELECT, INSERT, UPDATE, ou DELETE; et la clause WITH elle-même est attachée à un ordre primaire qui peut lui aussi être un SELECT, INSERT, UPDATE, ou DELETE. 

```sql
WITH max_count AS (
   SELECT count(*) c
   FROM zips.points p
   WHERE p.iso ='FR'
    )
SELECT p.zip, 100 * count(*)/ m.c 
FROM zips.points p , max_count m
WHERE p.iso ='FR'
GROUP BY p.zip, m.c
ORDER BY 2 DESC;
``` 
Avec cette exemple de sous-requêtes nous pouvons obtenir un COUNT qui sera utiliser au sein de la requêtes principal. Il est aussi possible de multiplier les WITH et les appeler dans le SELECT. 