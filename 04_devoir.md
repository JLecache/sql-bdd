# <span style="color:darkblue">Devoir<span>

Exercice de modélisation conceptuelle au niveau logique et passage au SQL

Le but étant d'imaginer un modèle conceptuel de données, de réaliser le schéma, d'élaborer les relations et cardinalités. Puis par la suite de faire le passage au SQL: Création du schéma, des tables, insertion de données, modification et suppression. 

Pour chaque étape SQL écrivez les requêtes dans un document

1. Imaginer et réaliser un modèle conceptuel de votre choix. Réaliser le schéma et les relations possibles entre vos tables. Créer des champs, des clés primaires et étrangères et les cardinalités

2. Expliquer vos choix et les relations possibles tout en répondant à un besoin client (imaginer les réponses possibles qu'un utilisateur peut vous poser)

3. Implémentez le tout en SQL 
- Créer les tables
- Créer les clés étrangères
- Insérer les données

4. Effectuez des sélections (SELECT) simples. Donnez des exemples de sélections 

5. Faire une suppression d'une ligne (DELETE)


Vous avez accès à toutes les ressources possibles et le support de cours : https://jlecache.gitlab.io/sql-bdd/00_intro.html

Installer PostgreSQL : 
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
https://postgresapp.com/ (pour mac)

**Rendu Dimanche 29 Septembre**
lecachejulien@gmail.com