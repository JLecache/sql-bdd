# <span style="color:darkblue">Examen<span>

Voici un dataset sur les aéroports comprenant les tables suivantes : 

**airports**: liste d'aéroports
**countries** : liste de pays
**navaids**: liste de stations d'aide à la navigation
**runways**: liste de piste d'atterrissage

A. Faites une lecture des tables, puis réalisez un schéma conceptuel de données, comprenant les relations, les cardinalités et les clés . Expliquez vos choix. 

B. Insérer les tables dans le SGBDR (par exemple dbeaver). Créer un nouveau schéma si nécessaire 

Pour chaque étape SQL écrivez les requêtes dans un document SQL provenant de Dbeaver et expliquez votre choix en détail.

1. Sélectionnez les aéroports situés en France

2. Sélectionnez les aéroports située en France, ayant pour type "small_airport", "large_airport" et "medium_airport"

3. Sélectionnez les aéroports ayant pour type "heliport" et se situant à plus de 10 000 ft (3048m)

4. Sélectionnez les aéroports ayant une latitude comprise entre 30 et 40

5. Sélectionnez les aéroports ayant un nom commençant par 'North'

6. La colonne "Continent" de la table Airports est NULL, faites un Update pour ajouter les bonnes valeurs de continent pour chaque aéroport. 

7. Sélectionnez les aéroports se situant dans le continent NA (North America) dans la table Airports

8. Créer une nouvelle table contenant les informations de lengh_ft (longueur) et width_ft (largueur) de la table runways et les colonnes de la table aiports: name, iso_country, continent. Chaque enregistrement doit correspondre. Nommez cette table "piste"

9. Supprimez les lignes qui ne possèdent pas de données dans "eleveation_ft" dans la table "airports"

10. Insérez un nouvel enregistrement de votre choix dans la table airports 


Vous avez accès à toutes les ressources possibles et le support de cours : https://jlecache.gitlab.io/sql-bdd/00_intro.html


**Rendu**
lecachejulien@gmail.com
