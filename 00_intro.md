# <span style="color:darkblue">Introduction<span>
    


Bienvenue dans le monde de la base de données relationnelle ! 

Ce cours vise à présenter les principes de fonctionnement des systèmes de gestion de base de données relationnelle, tant sur le plan de la modélisation de données que sur le plan logiciel. Il s'agit de comprendre les principes de base de la modélisation de données, d'étudier les fondamentaux du SQL, l'enjeu final étant de comprendre l'apport des systèmes de gestion de base de données pour le stockage de données alphanumériques et géographiques.

---------------------------
## <span style="color:darkblue">Séances<span>


- Mardi 10 septembre 9h30 - 18h : 
    - **SGBD : définition et principes de fonctionnement** 
    - **Modélisation de données avec Merise** 
    - **Merise en pratique**


- Mardi 17 septembre 9h30 - 18h : 
    - **Installation et administration d'une base de données**
    - **SQL comme standard**
        - **comprendre et interpréter le SQL**
        - **écrire ses premières requêtes**
         
- Mardi 01 octobre 10h30 - 18h :
    - **Pratique et échange libre**
    - **Evaluation** 



---------------------------
## <span style="color:darkblue">Setup<span>

Durant ce cours nous allons utiliser différents logiciels. Il est recommandé d'avoir déjà manipulé ArcGIS et QGIS. Les SGBDR suivants seront abordés ensemble : MySQL, pgAdmin et PostgreSQL

---------------------------
## <span style="color:darkblue">Mes contacts<span>


N'hésitez pas à me contacter via mon adresse mail 

    lecachejulien@gmail.com

---------------------------
## <span style="color:darkblue">Resources additionnel<span>



- https://sql.sh/
- pgAdmin 4 https://www.pgadmin.org/docs/pgadmin4/8.11/index.html
- PostgreSQL https://www.postgresql.org/
- MySQL https://www.mysql.com/
- Formation PostGIS  https://docs.3liz.org/formation-postgis/
- Clé étrangère en SQL : https://www.data-bird.co/blog/cle-etrangere
