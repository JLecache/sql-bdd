# <span style="color:darkblue">Système de gestion de base de données<span>

<div style="text-align: justify">

**Un Système de Gestion de Base de Données** (SGBD) est un logiciel système servant à stocker, à manipuler ou gérer, et à partager des données dans une base de données, en garantissant la qualité, la pérennité et la confidentialité des informations, tout en cachant la complexité des opérations. 


Parmi les logiciels les plus connus il est possible de citer : MySQL, PostgreSQL, SQLite, Oracle, Microsoft SQL Server, Access...

</div>


```{image} ./figures/MySQL.png
:alt: mySQL
:width: 150px
:align: left
``` 
```{image} ./figures/postgresql.png
:alt: figure_SGBD
:width: 150px
:align: center
``` 
```{image} ./figures/oracle.png
:alt: figure_SGBD
:width: 150px
:align: right
``` 
---

Il joue le rôle d’interface entre les utilisateurs de la base et les programmes d’applications. Le SGBD rend faciles l’organisation et l’accessibilité des données. 

Cet outil prend en charge: 
- la gestion des données
- le moteur de base de données 
- le schéma qui définit la structure logique des données. 

Ces trois éléments sont précieux pour assurer la sécurité, l’intégrité des données et l’uniformisation. Un SGBD permet l'administration de ce système ce qui inclut la gestion du changement, le suivi de la performance, la sauvegarde et la récupération des données, la gestion des utilisateurs et leurs droits.


```{image} ./figures/SGBD_1.png
:alt: figure_SGBD
:width: 750px
:align: center
``` 

---------------------------
## <span style="color:darkblue">Un SGBD relationnelle <span>

<div style="text-align: justify">
Une base de données relationnelle a l'avantage de nous donner la possibilité de créer une deuxième dimension à nos données. On parle alors de ligne et de colonnes. Selon ce modèle relationnel, une base de données consiste en une ou plusieurs relations. Là où les relations permettent d'introduire des interactions entre nos tables et de venir enrichir aisément nos données. 
 
```{image} ./figures/SGBDR_1.png
:alt: exemple_SBDR
:width: 750px
:align: center
``` 
Contrairement à un SGBD qui gère des bases de données sur un réseau informatique et des disques durs, une base de données SGBDR permet de maintenir les relations entre ses tables.

Les différences entre SGBD et SGBDR:

- Un SGBD n'autorise qu'un seul opérateur simultanément, alors que plusieurs utilisateurs peuvent exploiter un SGBDR simultanément. Un SGBDR est plus complexe ce qui permet à plusieurs utilisateurs d'accéder à la base de données tout en préservant simultanément l'intégrité des données, ce qui réduit considérablement le temps de réponse.

- La modification des données dans un SGBD est assez difficile, alors que vous pouvez facilement modifier les données dans un SGBDR à l'aide d'une requête SQL. Ainsi, les utilisateurs peuvent modifier / accéder à plusieurs éléments de données simultanément. Un SGBDR sera dont plus efficace.

- Un SGBDR peut gérer de gros volumes de données, un SBGD est approrié pour un petit volume.

- Un SGBD n'implique pas de clés et d'index, alors qu'un SGBDR spécifie une relation entre des éléments de données via des clés et des index.

- Comme un SGBD ne suit pas un modèle relationnelle, les données stockées peuvent présenter des incohérences. En revanche, un SGBDR sera structuré et cohérent.

Avantages du système de gestion de base de données relationnelle: Amélioration de la sécurité des données, Conserver la cohérence des données, Meilleure flexibilité et évolutivité, Entretien facile, Risque d'erreur réduit.

</div>

---------------------------
## <span style="color:darkblue">Langage SQL <span>

SQL ou **« Structured Query Language »** est un langage de programmation permettant de manipuler les données et les systèmes de bases de données relationnelles. SQL est donc un langage universel pour pouvoir communiquer avec les bases de données, cependant il existe certaines variantes. Le langage possède une norme internationale. 

```{image} ./figures/sql_schema.png
:alt: exemple_SBDR
:width: 750px
:align: center
``` 

Le SQL est un langage pour la **définition de données**, la **manipulation** et le **contrôle** de données. 

Les composantes principales du SQL sont :

- Créer une base de données
- Réaliser des tables dans une base de données
- Interroger ou demander des informations
- Insérer des enregistrements
- Mettre à jour ou modifier des enregistrements
- Supprimer des enregistrements
- Définir les permissions ou le contrôle d'accès dans la base de données pour la sécurité des données
- Définir des vues pour éviter de taper des requêtes complexes fréquemment utilisées

Il est possible d'effectuer d'autres tâches plus complexes avec un niveau plus avancé. 

Exemple de requêtes SQL

Intéroger la table client avec un select
```sql
SELECT ville FROM client WHERE iso ='FR' AND ville LIKE 'P%';
```
Insérer une nouvelle ligne au sein de la table client
```sql
INSERT INTO client VALUES ('valeur 1', 'valeur 2', ...)
```

---------------------------
## <span style="color:darkblue">SQL client<span>

Un administrateur de base de données doit souvent gérer des instructions SQL pour explorer la base de données pour diverses raisons.

- Interroger la base de données
- Construire et exécuter le code SQL
- Générer un rapport
- Prendre une sauvegarde
- Diagnostiquer le problème applicatif lié à la base de données

Nous allons utiliser DBeaver comme client, open-source il est disponible gratuitement. 

https://dbeaver.io/download/







